# make sure you've installed nodemon for autorestarts of the node process
# npm i nodemon -g --save

#watch and compile client code
#coffee -o public/ -b -cw src/ &

#watch and compile server code
#coffee --compile --stdio > server.js -cw src/server &

#continually run the server
nodemon server_w.js
