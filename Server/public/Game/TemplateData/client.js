﻿
var connection;
var logonTimeout;
var response = {result:false};
/*
var logonCallback = function(res)
{
    
    clearTimeout(logonTimeout);
    // Send Message back to Unity GameObject with name 'XXX' that has NetworkManager script attached
    SendMessage('Network','OnMatchJoined',JSON.stringify(res));
};

function Logon(str)
{
    var data = JSON.parse(str); 
    connection = io.connect();
    // Setup receiver client side function callback
    connection.on('JoinMatchResult', logonCallback);
    // Attempt to contact server with user data
    connection.emit('JoinMatchQueue', data);
    
    // Disconnect after 30 seconds if no response from server
    logonTimeout = setTimeout(function()
    {
        connection.disconnect();
        connection = null;
        response = {result:false};
        // Send Message back to Unity GameObject with name 'XXX' that has NetworkManager script attached
        SendMessage('Network','OnMatchJoined', JSON.stringify(response) ); 
    },30000);       
}
*/
function Init()
{
    console.log("Init function was called");
    connection = io.connect();
}

function OnEvent(objectname, eventname, functionname)
{
    console.log("OnEvent called: " + objectname + eventname + functionname);
    connection.on(eventname, function(response)
    {
        //console.log("Received Message " + eventname + " with " + JSON.stringify(response) );
        //SendMessage(objectname, functionname, JSON.stringify(response));
        SendMessage(objectname, functionname, JSON.stringify(response));
    });
}

function Emit(eventname, data)
{
    //console.log("Sending " + eventname + " data " + data);
    connection.emit(eventname, JSON.parse(data) );
}


