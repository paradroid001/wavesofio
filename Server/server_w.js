// Generated by CoffeeScript 1.10.0
var EntityData, GameServer, GameServerWrapper, PlayerData, StatsServer, _engine, _express, _gameapp, _port, _server, _stats, _statsapp, _statsport, start,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  hasProp = {}.hasOwnProperty;

EntityData = (function() {
  function EntityData(uid, x, y) {
    this.uid = uid;
    this.puid = -1;
    this.pos = {
      x: x,
      y: y,
      a: 0
    };
    this.vel = {
      x: 0,
      y: 0,
      a: 0
    };
    this.lastpos = {
      x: this.pos.x,
      y: this.pos.y,
      a: this.pos.a
    };
    this.lastUpdateTime = 0;
    this.xtra = "";
  }

  EntityData.prototype.update = function(data, time) {
    var dt;
    this.lastpos.x = this.pos.x;
    this.lastpos.y = this.pos.y;
    this.pos.x = data.pos.x;
    this.pos.y = data.pos.y;
    dt = this.lastUpdateTime - time;
    this.vel.x = (this.x - this.lastpos.x) * 1000 / dt;
    return this.vel.y = (this.y - this.lastpos.y) * 1000 / dt;
  };

  return EntityData;

})();

PlayerData = (function() {
  function PlayerData(id) {
    this.name = "Unknown";
    this.id = id;
    this.bullets = {};
    this.playerTypeID = "??";
    this.playerTypeName = "??";
    this.data = new EntityData(id, 0, 0);
  }

  PlayerData.prototype.add_bullet = function(id, data) {
    var bullet;
    bullet = new PositionData(data.x, data.y);
    return this.bullets[id] = bullet;
  };

  PlayerData.prototype.update_with_data = function(data, time) {
    return this.data.update(data, time);
  };

  return PlayerData;

})();

GameServer = (function() {
  function GameServer() {
    this.print_status = bind(this.print_status, this);
    this.remove_player = bind(this.remove_player, this);
    this.create_player = bind(this.create_player, this);
    this.get_new_id = bind(this.get_new_id, this);
    this.players = {};
    this.bullets = {};
    this.playerCount = 0;
    this.id = 0;
    this.timer = new Date();
  }

  GameServer.prototype.get_new_id = function() {
    return this.id++;
  };

  GameServer.prototype.create_player = function(playername) {
    var id, player;
    id = this.get_new_id();
    player = new PlayerData(id);
    player.name = playername;
    this.players[id] = player;
    this.playerCount += 1;
    return player;
  };

  GameServer.prototype.remove_player = function(id) {
    this.players[id] = null;
    delete this.players[id];
    return this.playerCount -= 1;
  };

  GameServer.prototype.print_status = function() {
    console.log("Players = " + this.playerCount);
    return console.log(this.players);
  };

  return GameServer;

})();

_express = require('express');

_gameapp = _express();

_statsapp = _express();

_engine = require('consolidate');

_server = null;

_stats = null;

_port = 8080;

_statsport = 8081;

StatsServer = (function() {
  function StatsServer(port) {
    this.update = bind(this.update, this);
    this.on_connection = bind(this.on_connection, this);
    var k, mem, memstats, v, versionstats;
    this.port = port;
    this.http = require('http').Server(_statsapp);
    this.io = require('socket.io')(this.http);
    this.io.on('connection', this.on_connection);
    this.http.listen(this.port);
    console.log("Stats Server listening on port " + this.port);
    console.log("This processor architecture is " + process.arch);
    mem = process.memoryUsage();
    memstats = (function() {
      var results;
      results = [];
      for (k in mem) {
        v = mem[k];
        results.push(k + " = " + v);
      }
      return results;
    })();
    console.log("Memory usage (bytes): " + memstats);
    console.log("Uptime: " + (process.uptime()));
    versionstats = (function() {
      var ref, results;
      ref = process.versions;
      results = [];
      for (k in ref) {
        v = ref[k];
        results.push(k + " = " + v + "\n");
      }
      return results;
    })();
    console.log("Version: " + versionstats);
    this.maxDelay = 100;
    this.lastTime = process.hrtime();
    this.delayStats = [];
  }

  StatsServer.prototype.get_hr_diff_time = function(time) {
    var ts;
    ts = process.hrtime(time);
    return (ts[0] * 1000) + (ts[1] / 1000000);
  };

  StatsServer.prototype.on_connection = function(socket) {
    return socket.emit('connected', {});
  };

  StatsServer.prototype.update = function(players, bullets, time, interval) {
    var delay, k;
    delay = this.get_hr_diff_time(this.lastTime) - interval;
    this.lastTime = process.hrtime();
    this.delayStats.push(delay);
    return this.io.emit('update', {
      playerCount: ((function() {
        var results;
        results = [];
        for (k in players) {
          if (!hasProp.call(players, k)) continue;
          results.push(k);
        }
        return results;
      })()).length,
      delay: delay
    });
  };

  return StatsServer;

})();

GameServerWrapper = (function() {
  function GameServerWrapper(port, statsport) {
    this.on_connection = bind(this.on_connection, this);
    this.update_stats = bind(this.update_stats, this);
    this.http = require('http').Server(_gameapp);
    this.io = require('socket.io')(this.http);
    this.io.on('connection', this.on_connection);
    this.port = port;
    this.statsport = statsport;
    this.timer = new Date();
    this.http.listen(this.port);
    console.log("Game Server listening on port " + this.port);
    setInterval(this.update_stats, 1000);
    this.server = new GameServer();
    this.server.print_status();
  }

  GameServerWrapper.prototype.update_stats = function() {
    return _stats.update(this.server.players, this.server.bullets, this.timer.getTime(), 1000);
  };

  GameServerWrapper.prototype.on_connection = function(socket) {
    var on_disconnect, on_join, on_update;
    console.log("connection!");
    setTimeout((function(_this) {
      return function() {
        return socket.emit('connected', {
          a: 1
        });
      };
    })(this), 1500);
    on_disconnect = (function(_this) {
      return function(data) {
        console.log("Disconnection data is " + data);
        console.log("disconnect from player: " + socket.player);
        if ((socket.player != null)) {
          socket.player.data.xtra = {
            local: false
          };
          _this.server.remove_player(socket.player.data.uid);
          _this.io.emit('player_disconnected', socket.player.data);
        }
        socket.player = null;
        return _this.server.print_status();

        /*
        theuser = socket.user
        if (theuser != null)
            console.log(socket.user.uid + " disconnected")
        else
            console.log("disconnection, could not get user from socket");
         */
      };
    })(this);
    on_update = (function(_this) {
      return function(data) {
        var player;
        player = _this.server.players["" + data.uid];
        if (player != null) {
          player.update_with_data(data, _this.timer.getTime());
        }
        data.xtra = null;
        return socket.broadcast.emit('updated', data);
      };
    })(this);
    on_join = (function(_this) {
      return function(playername) {
        var existingplayer, newplayer, ref, uid;
        console.log("on_join, name = " + playername);
        newplayer = _this.server.create_player(playername);
        socket.player = newplayer;
        newplayer.data.xtra = {
          type: "NonLocalPlayer",
          name: "abcdef" + newplayer.data.uid,
          local: false
        };
        socket.broadcast.emit('joined', newplayer.data);
        newplayer.data.xtra = {
          type: "Player",
          name: playername,
          local: true
        };
        socket.emit('joined', newplayer.data);
        newplayer.data.xtra = null;
        ref = _this.server.players;
        for (uid in ref) {
          existingplayer = ref[uid];
          if (uid !== ("" + newplayer.data.uid)) {
            console.log("existing uid " + uid + " did not match " + newplayer.data.uid);
            if (existingplayer === null) {
              console.log("Player as " + uid + " is null");
            } else {
              existingplayer.data.xtra = {};
              existingplayer.data.xtra.type = "NonLocalPlayer";
              existingplayer.data.xtra.name = existingplayer.name;
              existingplayer.data.xtra.local = false;
              socket.emit('joined', existingplayer.data);
              existingplayer.data.xtra = null;
            }
          }
        }
        return _this.server.print_status();
      };
    })(this);
    socket.on('disconnect', on_disconnect);
    socket.on('join', on_join);
    return socket.on('update', on_update);
  };

  return GameServerWrapper;

})();

start = function() {
  _server = new GameServerWrapper(_port);
  _stats = new StatsServer(_statsport);
  _gameapp.use(_express["static"](__dirname + '/public/Game'));
  _gameapp.set('views', __dirname + '/public/Game');
  _gameapp.engine('html', _engine.mustache);
  _gameapp.set('view engine', 'html');
  _gameapp.get('/', function(req, res) {
    return res.render('index.html');
  });
  _statsapp.use(_express["static"](__dirname + '/public'));
  _statsapp.set('views', __dirname + '/public');
  _statsapp.engine('html', _engine.mustache);
  _statsapp.set('view engine', 'html');
  return _statsapp.get('/stats', function(req, res) {
    return res.render('server.html');
  });
};

start();
