_game = null
_namebox = null
_playbutton = null
_serverbox = null
_nextbutton = null
_prevbutton = null

class GameTitleScene extends RBPreLoaderScene
    constructor: () ->
        super("Title Scene")
        #@input.add(RB_KEY_UP, @.on_key_up)
        @loading_text = new RBText(100, 100, "Loading")
        @continue_text = new RBText(100, 130, "Continue")
        @stage.add_child(@loading_text)
        @stage.add_child(@continue_text)
        _playbutton.show()
        _namebox.show()
        _serverbox.show()
        _prevbutton.show()
        _nextbutton.show()
        
        @playertypenum = 0
        @previewplayer = null
        
        _nextbutton.click( () =>
            @next_player_type()
        )

        _prevbutton.click( () =>
            @prev_player_type()
        )

        _playbutton.click( () =>
            console.log("play clicked")
            @.pop()
            _playbutton.hide()
            _namebox.hide()
            _serverbox.hide()
            _prevbutton.hide()
            _nextbutton.hide()
        )

    next_player_type: () ->
        @playertypenum +=1
        @playertypenum = @playertypenum % PlayerType.len
        @create_preview()

    prev_player_type: () ->
        @playertypenum -=1
        if @playertypenum < 0
            @playertypenum = PlayerType.len + @playertypenum
        @create_preview()


    create_preview: () ->
        console.log("Player type num is now #{@playertypenum}")
        if @previewplayer != null
            @stage.remove_child(@previewplayer)
            @previewplayer.destroy()
        @previewplayer = new PlatformerChar(300, 400, @playertypenum)
        @previewplayer.timeout = 0 #don't timeout
        @stage.add_child(@previewplayer)
        @previewplayer.physics.deactivate()
        @previewplayer.set_name(PlayerType.names(@playertypenum))

    on_preloaded:() ->
        @continue_text.set_text("Press Space to continue")
        @create_preview()
        
    ###
    on_key_up: (event) =>
        code = event.keyCode
        keys = @_RB.input.keys
        if code is keys.space
            @.pop()
    ###

    update: (dt) ->
        super(dt)
        @loading_text.set_text("Loading...[#{@.pl.stats.total}]")

class GameScene extends RBScene
    constructor: () ->
        super("Game Scene")
        console.log("Name is " + @playername)
        @envgroup = new RBLayer()
        @envgroup.name = "Environment"
        @playergroup = new RBLayer()
        @playergroup.name = "Player"
        @bg1group = new RBLoopLayer(200, 200)
        @bg1group.name = "BG1"
        @bg2group = new RBLoopLayer(400, 0)
        @bg2group.name = "BG2"
        @bg2group.set_parallax(1.5, 0.5)

        @bulletgroup = new RBLayer()
        @bulletgroup.name = "Bullets"
        RBV(RBV_GAME).bulletgroup = @bulletgroup


        @input.add(RB_KEY_UP, @.on_key_up)
        @input.add(RB_KEY_DOWN, @.on_key_down)

        @stage.add_child(@bg1group)
        @stage.add_child(@bg2group)
        @stage.add_child(@envgroup)
        @stage.add_child(@bulletgroup)
        @stage.add_child(@playergroup)

        @envgroup.add_child( new PlatformGrass(100, 200) )
        @envgroup.add_child( new PlatformGrass(132, 200) )

        @cam = new RBCamera(0, 0, RB().screen().width, RB().screen().height)
        #@cam.follow(@player)
        
        @editor = new RBEditor(32, 32,
            [@envgroup, @bg1group, @bg2group],
            {
                "Grass" : PlatformGrass
                "Steel" : PlatformSteel
                "Sky" : PlatformSky
                "Cloud1" : PlatformCloud1
                "Cloud2" : PlatformCloud2
                "Cloud3" : PlatformCloud3
                "RockLeft" : PlatformRockLeft
                "RockTopLeft" :PlatformRockTopLeft
                "RockBottomLeft": PlatformRockBottomLeft
                "RockRight": PlatformRockRight
                "RockTopRight": PlatformRockTopRight
                "RockBottomRight": PlatformRockBottomRight
                "RockTop": PlatformRockTop
                "RockBottom": PlatformRockBottom
            })
        @.add_child(@editor) #make it get updated
        @editor.load_map_from_file("map.json")

        fadeinaction = new RBFadeIn(2.0)
        fadeinaction.run_on(@stage)
        @stage.set_alpha(0)
        console.log("finished constructing scene")

    add_entity: (p, pname, isPlayer = false) ->
        @playergroup.add_child(p)
        p.set_name(pname)
        if isPlayer
            @player = p
            @cam.follow(p)

    finish: () ->
        console.log("Game finished")
        @.pop(true)

    on_key_down: (event) =>
        keys = @_RB.input.keys
        switch (event.keyCode)
            when keys.right
                @player.go_right(true)
                @_RB.input.swallow(event)
            when keys.left
                @player.go_left(true)
                @_RB.input.swallow(event)
            when keys.up
                @player.jump()

            when keys.space
                @player.shoot()
            when keys.esc
                @.finish()
                @_RB.input.swallow(event)
    
    on_key_up: (event) =>
        keys = @_RB.input.keys
        switch (event.keyCode)
            when keys.right
                @player.go_right(false)
                @_RB.input.swallow(event)
            when keys.left
                @player.go_left(false)
                @_RB.input.swallow(event)

    update: (dt) ->
        super(dt) #updates stage, editor, etc
        @cam.update(dt)
        @_RB.collide(@envgroup.members, @playergroup.members)

    draw: () ->
        #For normal drawing (no camera)
        #@stage.render()
        #@editor.render()
        
        #When using the camera
        @cam.draw(@stage)
        @cam.draw(@editor)


class GameApp extends RBGame
    constructor: () ->
        super()
        @titlescene = @gamescene = null
        @_RB.events.add(RB_EVENT_ON_START, @.show_title)
        @players = {}
        @bullets = {}
        @socket = null
        @uiplayers = null
        @playername = null
        @playertype = null

    show_title: () =>
        if @titlescene is null
            @_RB.events.remove(RB_EVENT_ON_START, @.show_title)
            @titlescene = new GameTitleScene()
            @titlescene.events.add(RB_EVENT_ON_SCENE_EXIT, @.start_game)
        @titlescene.push()

    start_game: () =>
        @playertype = @titlescene.playertypenum

        if @gamescene is null
            @_RB.events.remove(RB_EVENT_ON_SCENE_EXIT, @.start_game)
            @_RB.events.add(RB_EVENT_ON_SCENE_EXIT, @.game_over)
            @gamescene = new GameScene()
        @serverip = _serverbox.val()
        console.log("Connecting to #{@serverip}:8080")
        @socket = io.connect("http://#{@serverip}:8080")
        @uiplayers = document.getElementById("players")
        @uiplayerlist = document.getElementById("playerlist")
        @playername = _namebox.val()
        

        @socket.on('count', (data) =>
            @uiplayers.innerHTML = "Players: " + data['playerCount']
            console.log(data['players'])
            names = for id, name of data['players']
                "#{name}"
            console.log("Names are now: #{names}")
            @uiplayerlist.innerHTML = names
        )

        @socket.on('connected', (data) =>
            selfId = data['playerId']
            console.log("Connected!, #{selfId}, previously got playertype of #{@playertype}")
            player = new Player(100, 100, @socket, selfId, @playertype)
            @gamescene.add_entity(player, @playername, true)
            @players[selfId] = player
            @socket.emit('connect_finish', {'playerId': selfId, 'playerName': @playername})
        )

        @socket.on('updated', (data) =>
            #console.log(data)
            #return
            actorid = data['playerId']
            actorname = data['playerName']
            actortype = data['playerType']
            x = data['x']
            y = data['y']

            if actortype != PlayerType.bullet
                actor = @players[actorid]
            else
                actor = @bullets[actorid]
                #console.log(actorid, actor)
                
            if actor? and actor isnt null
                actor.x = x
                actor.y = y
                actor.updated = true
            else
                if actortype != PlayerType.bullet
                    console.log("New Player: type is #{actortype}")
                    np = new NonPlayer(x, y, actorid, actortype)
                    @gamescene.add_entity(np, actorname, false)
                    @players[actorid] = np
                else
                    #it's a bullet owned by ownerId
                    console.log("New bullet")
                    #actorowner = data['ownerId']
                    b = new Bullet(x, y)
                    bulletgroup = RBV(RBV_GAME).bulletgroup
                    bulletgroup.add_child(b)
                    @bullets[actorid] = b
        )


        @gamescene.push()
    
    game_over: () =>
        console.log("Game over - restarting")
        if @gamescene isnt null
            @gamescene.destroy()
        @.show_title()


initApp = () ->
    if not (_game is null)
        return #prevent multiple inits
    
    _playbutton = $("#playbutton")
    _namebox = $("#playername")
    _serverbox = $("#serverip")
    _prevbutton = $("#player_prev")
    _nextbutton = $("#player_next")

    #Engine Settings (RBV_ENG)
    RBV(RBV_ENG, {
        fps: 60
        screenwidth: 512
        screenheight: 512
        canvaswidth: 512
        canvasheight: 512
        canvasid: "canvas"
        canvassmoothing: false
    })
    #Image Settings (RBV_IMG)
    RBV(RBV_IMG, {
        envimage: "img/env_tiles.png"
        warrior_m_image: "img/warrior_m.png"
        warrior_f_image: "img/warrior_f.png"
        mage_m_image: "img/mage_m.png"
        mage_f_image: "img/mage_f.png"
        healer_m_image: "img/healer_m.png"
        healer_f_image: "img/healer_f.png"
        ninja_m_image: "img/ninja_m.png"
        ninja_f_image: "img/ninja_f.png"
        ranger_m_image: "img/ranger_m.png"
        ranger_f_image: "img/ranger_f.png"
        priest_m_image: "img/townfolk1_m.png"
        priest_f_image: "img/townfolk1_f.png"
        bulletsimage: "img/bullets.png"

    })
    #Sound Settings (RBV_SND)
    RBV(RBV_SND, {
    #   laser: "data/sfx.wav"
    })
    #Sprite Settings (RBV_SS)
    RBV(RBV_SS, {
        env: ["env", RBV(RBV_IMG).envimage, 10, 10]
        warrior_m: ["warrior_m", RBV(RBV_IMG).warrior_m_image, 3, 4]
        warrior_f: ["warrior_f", RBV(RBV_IMG).warrior_f_image, 3, 4]

        healer_m: ["healer_m", RBV(RBV_IMG).healer_m_image, 3, 4]
        healer_f: ["healer_f", RBV(RBV_IMG).healer_f_image, 3, 4]

        ninja_m: ["ninja_m", RBV(RBV_IMG).ninja_m_image, 3, 4]
        ninja_f: ["ninja_f", RBV(RBV_IMG).ninja_f_image, 3, 4]

        ranger_m: ["ranger_m", RBV(RBV_IMG).ranger_m_image, 3, 4]
        ranger_f: ["ranger_f", RBV(RBV_IMG).ranger_f_image, 3, 4]

        priest_m: ["priest_m", RBV(RBV_IMG).priest_m_image, 3, 4]
        priest_f: ["priest_f", RBV(RBV_IMG).priest_f_image, 3, 4]

        mage_m: ["mage_m", RBV(RBV_IMG).mage_m_image, 3, 4]
        mage_f: ["mage_f", RBV(RBV_IMG).mage_f_image, 3, 4]

        bullets: ["bullets", RBV(RBV_IMG).bulletsimage, 4, 4]

    })
    #Game Settings (RBV_GAME)
    RBV(RBV_GAME, {
        playerwidth: 35
        playerheight: 40
        enemywidth: 35
        enemyheight: 40
        bulletwidth: 8
        bulletheight: 8
    })

    
    _game = new GameApp()
    RB().start()

