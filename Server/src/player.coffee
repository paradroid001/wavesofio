class PlayerType
    @bullet: -1
    #Actual player types
    @healer_m: 0
    @healer_f: 1
    @mage_m:   2
    @mage_f:   3
    @ninja_m:  4
    @ninja_f:  5
    @ranger_m: 6
    @ranger_f: 7
    @priest_f: 8
    @priest_m: 9
    @warrior_m: 10
    @warrior_f: 11
    @len: 12 #keep this updated
    @names: (id) ->
        n = ["Healer", "Mage", "Ninja", "Ranger", "Priest", "Warrior"]
        return n[(id/2)|0]

class EntityType
    @bullet: 0
    @player: 1

class NetworkedEntity extends RBSprite
    constructor: (X, Y, img, w, h) ->
        super(X, Y, img, w, h)
        @is_local = false
        @entityID = null
        @networkData = {}
        @updated = true
        @timeSinceLastUpdate = 0
        @timeout = 3000

    local_init: () ->
        @is_local = true

    net_data: () ->
        r=
            playerId: @entityID
            x: @x
            y: @y
        return r

    update: (dt) ->
        super(dt)
        if @is_local
            #@socket.emit('update', {"playerId": @entityID, "playerName": @nametext.text, "playerType": @playertype, "x": @x, "y": @y} )
            @socket.emit('update', @net_data())
        else
            #if we haven't received an update for X seconds,
            #destroy ourselves
            #unless timeout == 0 (which means we don't care about timeouts)
            if @timeout != 0
                if @updated
                    @timeSinceLastUpdate = 0
                    @updated = false
                else
                    @timeSinceLastUpdate += dt
                    if @timeSinceLastUpdate > @timeout
                        console.log("player #{@playerid} timed out")
                        @.destroy()


            

class PlatformerChar extends NetworkedEntity
    constructor:( X, Y, @playertype=PlayerType.healer_m) ->
        if !@playertype?
            @playertype = PlayerType.healer_m
        img = null
        ssimage = null
        console.log("Player type is #{@playertype}")

        switch(@playertype)
            when PlayerType.warrior_m
                img = RBV_IMG.warrior_m_image
                ssimage = RBV_SS.warrior_m
            when PlayerType.warrior_f
                img = RBV_IMG.warrior_f_image
                ssimage = RBV_SS.warrior_f

            when PlayerType.mage_m
                img = RBV_IMG.mage_m_image
                ssimage = RBV_SS.mage_m
            when PlayerType.mage_f
                img = RBV_IMG.mage_f_image
                ssimage = RBV_SS.mage_f

            when PlayerType.healer_m
                img = RBV_IMG.healer_m_image
                ssimage = RBV_SS.healer_m
            when PlayerType.healer_f
                img = RBV_IMG.healer_f_image
                ssimage = RBV_SS.healer_f

            when PlayerType.ninja_m
                img = RBV_IMG.ninja_m_image
                ssimage = RBV_SS.ninja_m
            when PlayerType.ninja_f
                img = RBV_IMG.ninja_f_image
                ssimage = RBV_SS.ninja_f

            when PlayerType.priest_m
                img = RBV_IMG.priest_m_image
                ssimage = RBV_SS.priest_m
            when PlayerType.priest_f
                img = RBV_IMG.priest_f_image
                ssimage = RBV_SS.priest_f

            when PlayerType.ranger_m
                img = RBV_IMG.ranger_m_image
                ssimage = RBV_SS.ranger_m
            when PlayerType.ranger_f
                img = RBV_IMG.ranger_f_image
                ssimage = RBV_SS.ranger_f

        super(X, Y, img, RBV_GAME.playerwidth, RBV_GAME.playerheight)
        @ss = RB().media.get_spritesheet(ssimage)
        @.add_animation("default", new RBAnimation(@ss, [6, 7, 8, 7], RB_ANIM_LOOP, 100) )
        @.add_animation("walk_right", new RBAnimation(@ss, [3, 4, 5, 4], RB_ANIM_LOOP, 100) )
        @.add_animation("walk_left", new RBAnimation(@ss, [9, 10, 11, 10], RB_ANIM_LOOP, 100) )

        @.set_animation("default")

        #add in physics
        @physics = new RBPhysics()
        @.add_component(@physics)

        #super(X, Y, imagename, W, H)
        #console.log("#{X}, #{Y}, #{imagename}, #{W}, #{H}") 
        @on_ground = false

        @nametext = new RBText(0, -25, "????")
        @.add_child(@nametext)
        
    hit_bottom: (robj) =>
        if robj instanceof PlatformerPlatform and @physics.velocity.y > 0
            @physics.velocity.y = 0
            @on_ground = true
            @.adjust_bottom(robj.top)

    hit_right: (robj) =>
        if robj instanceof PlatformSteel
            @physics.velocity.x = 0
            @on_ground = false
            @.adjust_right(robj.left)
    hit_left: (robj) =>
        if robj instanceof PlatformSteel
            @physics.velocity.x = 0
            @on_ground = false
            @.adjust_left(robj.right)

    set_name: (n) ->
        @nametext.text = n

            


class Player extends PlatformerChar
    constructor: (X, Y, @socket, @playerid, ptype) ->
        super(X, Y, ptype)
        console.log("creatng anims, spritesheet is #{@ss}")
        @facing_right = false
        @speed = 150
        @local_init()
        console.log("end player constructor")

    go_right: (should_move = true) ->
        if (should_move)
            @facing_right = true
            @physics.velocity.x = @speed
            @set_animation("walk_right")
        else
            @physics.velocity.x = 0
    
    go_left: (should_move = true) ->
        if (should_move)
            @facing_right = false
            @physics.velocity.x = -@speed
            @set_animation("walk_left")
        else
            @physics.velocity.x = 0

    jump: () ->
        if @on_ground
            @on_ground = false
            @physics.velocity.y = -2*@speed
    
    shoot: () ->
        bulletgroup = RBV(RBV_GAME).bulletgroup
        shootangle = -90
        if @facing_right
            shootangle = 90

        b = new Bullet(@x, @y)
        b.local_init(shootangle, @socket, @)
        bulletgroup.add_child(b)
            
    net_data: () ->
        r = super()
        r['playerName'] = @nametext.text
        r['playerType'] = @playertype
        return r

    update: (dt) ->
        super(dt)
        #@socket.emit('update', {"playerId": @playerid, "playerName": @nametext.text, "playerType": @playertype, "x": @x, "y": @y} )
        
class NonPlayer extends PlatformerChar
    constructor: (X, Y, @playerid, playertype) ->
        super(X, Y, playertype)
        @updated = true
        @timeSinceLastUpdate = 0

    update: (dt) ->
        super(dt)
        
        #if we haven't received an update for X seconds,
        #destroy ourselves
        if @updated
            @timeSinceLastUpdate = 0
            @updated = false
        else
            @timeSinceLastUpdate += dt
            if @timeSinceLastUpdate > 3000
                console.log("player #{@playerid} timed out")
                @.destroy()



class Bullet extends NetworkedEntity
    constructor: (X, Y) -> #, @shootangle, @owner) ->
        super(X, Y, RBV_IMG.bulletsimage, RBV_GAME.bulletwidth, RBV_GAME.bulletheight)
        @speed = 250
        @.add_tag("bullet")
        @solid = true
        ss = RB().media.get_spritesheet(RBV_SS.bullets)
        @.add_animation("default", new RBAnimation(ss, [2,6,10,14,10,6, 2, 2, 2, 2], RB_ANIM_LOOP, 50))
        @.set_animation("default")
        
        @headingvector = new RBPoint(0, 0)
        
    local_init: (@shootangle, @socket, @owner) ->
        super()
        @headingvector.cartesian(@shootangle, 1)
        console.log("Headingvector is #{@headingvector.x}, #{@headingvector.y}")

        #bullets only last 2 seconds
        deathaction = new RBActionSequence([new RBDelay(2.0), new RBCallFunction("destroy")])
        deathaction.run_on(@)

    net_data: () ->
        r = super()
        r["playerType"] = PlayerType.bullet
        return r

    update: (dt) ->
        super(dt)
        if (@is_local)
            @.move_by(@headingvector.x * (dt * @speed/1000), @headingvector.y * (dt * @speed / 1000) )

class PlatformerTile extends RBSprite
    constructor: (X, Y, frames) ->
        super(X, Y, RBV_IMG.envimage, 32, 32)
        ss = RB().media.get_spritesheet(RBV_SS.env)
        #console.log(ss)
        @.add_animation("default", new RBAnimation(ss, frames, RB_ANIM_NORMAL) )
        @.set_animation("default")

class PlatformerPlatform extends PlatformerTile
    constructor: (X, Y, frames) ->
        super(X, Y, frames)
        @solid = true

class PlatformGrass extends PlatformerPlatform
    constructor: (X, Y) ->
        super(X, Y, [2])
class PlatformSteel extends PlatformerPlatform
    constructor: (X, Y) ->
        super(X, Y, [14])

class PlatformSky extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [3])
class PlatformCloud1 extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [4])
class PlatformCloud2 extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [5])
class PlatformCloud3 extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [6])
class PlatformRockTopLeft extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [11])
class PlatformRockTop extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [12])
class PlatformRockTopRight extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [13])
class PlatformRockLeft extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [21])
class PlatformRockCentre extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [22])
class PlatformRockRight extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [23])
class PlatformRockBottomLeft extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [31])
class PlatformRockBottom extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [32])
class PlatformRockBottomRight extends PlatformerTile
    constructor: (X, Y) ->
        super(X, Y, [33])


