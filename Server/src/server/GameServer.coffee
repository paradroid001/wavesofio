class EntityData
    constructor: (uid, x, y) ->
        @uid = uid
        @puid = -1 #no parent
        @pos = {x: x, y: y, a: 0}
        @vel = {x: 0, y: 0, a: 0}
        @lastpos = {x: @pos.x, y: @pos.y, a: @pos.a}
        @lastUpdateTime = 0
        @xtra = ""

    update: (data, time) ->
        @lastpos.x = @pos.x
        @lastpos.y = @pos.y
        @pos.x = data.pos.x
        @pos.y = data.pos.y
        dt = @lastUpdateTime - time
        @vel.x = (@x - @lastpos.x) * 1000 / dt
        @vel.y = (@y - @lastpos.y) * 1000 / dt
        

class PlayerData
    constructor: (id) ->
        @name = "Unknown"
        @id = id
        @bullets = {} #list of bullets by ID
        @playerTypeID = "??"
        @playerTypeName = "??"
        @data = new EntityData(id, 0, 0)

    add_bullet: (id, data) ->
        bullet = new PositionData(data.x, data.y)
        @bullets[id] = bullet

    update_with_data: (data, time) ->
        @data.update(data, time)

class GameServer
    constructor: () ->
        @players = {}
        @bullets = {}
        @playerCount = 0
        @id = 0
        @timer = new Date()

    get_new_id: () =>
        return @id++

    create_player: (playername) =>
        id = @get_new_id()
        player = new PlayerData(id)
        player.name = playername
        @players[id] = player
        @playerCount += 1
        return player

    remove_player: (id) =>
        @players[id] = null
        delete @players[id]
        @playerCount -= 1

    print_status: () =>
        console.log("Players = #{@playerCount}")
        console.log(@players)
