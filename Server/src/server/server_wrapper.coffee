_express = require('express')

_gameapp = _express()
_statsapp = _express()

_engine = require('consolidate');
_server = null
_stats = null
_port = 8080
_statsport = 8081

class StatsServer
    constructor: (port) ->
        @port = port
        @http = require('http').Server(_statsapp)
        @io = require('socket.io')(@http)
        @io.on('connection', @on_connection)
        @http.listen(@port)
        console.log("Stats Server listening on port #{@port}")
        console.log("This processor architecture is #{process.arch}")
        #console.log("CPU Usage: #{process.cpuUsage()}") #only available 6.1 onwards
        mem = process.memoryUsage()
        memstats = for k, v of mem
            "#{k} = #{v}"
        console.log("Memory usage (bytes): #{memstats}")
        console.log("Uptime: #{process.uptime()}")
        versionstats = for k, v of process.versions
            "#{k} = #{v}\n"
        console.log("Version: #{versionstats}")

        #setInterval(@update, 1000)
        @maxDelay = 100 #100ms?
        @lastTime = process.hrtime()
        @delayStats = []

    get_hr_diff_time: (time) ->
        ts = process.hrtime(time) #gives [seconds, nanoseconds]
        return (ts[0] * 1000) + (ts[1] / 1000000) #convert to milliseconds/nanoseconds


    on_connection: (socket) =>
        socket.emit('connected', {})

    update: (players, bullets, time, interval) =>
        delay = @get_hr_diff_time(@lastTime) - interval
        @lastTime = process.hrtime()
        #console.log("delay is #{delay}")
        @delayStats.push(delay)
        #console.log("Players is #{players}, #{players.length}")
        @io.emit('update', {playerCount: (k for own k of players).length, delay: delay })


class GameServerWrapper
    constructor: (port, statsport) ->
        @http = require('http').Server(_gameapp)
        @io = require('socket.io')(@http)
        @io.on('connection', @on_connection)
        @port = port
        @statsport = statsport
        @timer = new Date()
        @http.listen(@port)
        console.log("Game Server listening on port #{@port}")
        setInterval(@update_stats, 1000)
        @server = new GameServer()
        @server.print_status()
    update_stats: () =>
        _stats.update(@server.players, @server.bullets, @timer.getTime(), 1000)


    on_connection: (socket) =>
        console.log("connection!")
        #wait 1.5 seconds before replying
        setTimeout( () =>
            #socket.emit('connected', {uid: newplayer.id})
            socket.emit('connected', {a: 1} )
            #console.log("Sent connection data for #{newplayer.id}")
        , 1500)
        
        on_disconnect = (data) =>
            console.log("Disconnection data is #{data}")
            console.log("disconnect from player: #{socket.player}")
            if (socket.player?)
                socket.player.data.xtra = {local: false}
                #emit to all sockets
                @server.remove_player(socket.player.data.uid)
                @io.emit('player_disconnected', socket.player.data)
            socket.player = null
            #@io.emit(playerCount: @server.playerCount, players: @server.players)
            @server.print_status()
            ###
            theuser = socket.user
            if (theuser != null)
                console.log(socket.user.uid + " disconnected")
            else
                console.log("disconnection, could not get user from socket");
            ###
        
        on_update = (data) =>
            #console.log(data)
            #So there are several things we want to check for
            #when we get updates.
            #
            #We want to check a number of hacks:
            #1. Firstly, have players moved too far? Past their maximum speed?
            #
            #2. Secondly, have they fired more bullets than they should be able to?
            #
            #3. Thirdly, any bullet hits need to be checked - were they even in the
            #   right ballpark?
            #
            #console.log(data);
            player = @server.players["#{data.uid}"]
            if player?
                player.update_with_data(data, @timer.getTime() )
            
            #null out xtra...debugging the http issues
            data.xtra = null
            socket.broadcast.emit('updated', data) #go to everything but us
            #@io.emit('updated', data)


        on_join = (playername) =>
            console.log("on_join, name = " + playername)
            newplayer = @server.create_player(playername)
            socket.player = newplayer
            #@io.emit('joined', newplayer.data)
            #to everyone else
            newplayer.data.xtra = {type: "NonLocalPlayer", name: "abcdef#{newplayer.data.uid}", local: false}
            socket.broadcast.emit('joined', newplayer.data)
            #Back to the client
            newplayer.data.xtra = {type: "Player", name: playername, local: true}
            socket.emit('joined', newplayer.data)
            
            #Null the xtra data so we don't keep sending it all the time.
            newplayer.data.xtra = null
            #and now, also back to the client, every other player.
            for uid, existingplayer of @server.players
                if uid != "#{newplayer.data.uid}"
                    console.log("existing uid #{uid} did not match #{newplayer.data.uid}")
                    if existingplayer == null
                        console.log("Player as #{uid} is null")
                    else
                        existingplayer.data.xtra = {}
                        existingplayer.data.xtra.type = "NonLocalPlayer"
                        existingplayer.data.xtra.name = existingplayer.name
                        existingplayer.data.xtra.local = false
                        socket.emit('joined', existingplayer.data)
                        existingplayer.data.xtra = null
            @server.print_status()

        socket.on('disconnect', on_disconnect)
        socket.on('join', on_join)
        socket.on('update', on_update)
    

start = () ->
    _server = new GameServerWrapper(_port)
    _stats = new StatsServer(_statsport)
    _gameapp.use(_express.static(__dirname + '/public/Game') )
    _gameapp.set('views', __dirname + '/public/Game')
    _gameapp.engine('html', _engine.mustache)
    _gameapp.set('view engine', 'html')
    _gameapp.get('/', (req, res) ->
        res.render('index.html')
    )
    
    _statsapp.use(_express.static(__dirname + '/public') )
    _statsapp.set('views', __dirname + '/public')
    _statsapp.engine('html', _engine.mustache)
    _statsapp.set('view engine', 'html')
    _statsapp.get('/stats', (req, res) ->
        res.render('server.html')
    )

start()
