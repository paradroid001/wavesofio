class StatsApp
    constructor: (serverip, serverport) ->
        #console.log("Connecting to #{serverip}, #{serverport}")
        @socket = io.connect("http://#{serverip}:#{serverport}")
        
        ##This code runs when everything has been loaded on the page */
        ## Inline sparklines take their values from the contents of the tag
        $('.inlinesparkline').sparkline()

        ##Sparklines can also take their values from the first argument 
        ##passed to the sparkline() function
        @myvalues = []
        @delayvalues = []
        $('.dynamicsparkline').sparkline(@myvalues)

        ##The second argument gives options such as chart type */
        $('.dynamicbar').sparkline(@myvalues, {type: 'bar', barColor: 'green'} )

        ##Use 'html' instead of an array of values to pass options 
        ##to a sparkline with data in the tag
        $('.inlinebar').sparkline('html', {type: 'bar', barColor: 'red'} )
        console.log("finished initing stats app")

        @socket.on('connected', (data) =>
            console.log("Connected to stats server")
        )

        @socket.on('update', (data)  =>
            @myvalues.push(data.playerCount)
            @delayvalues.push(data.delay)
            $('.dynamicsparkline').sparkline(@myvalues)
            $('.serverload').sparkline(@delayvalues, {normalRangeMin: 0, normalRangeMax: 100, height: 100})
        )

_statsApp = null
initApp = () ->
    _statsApp = new StatsApp("127.0.0.1", 8081)
