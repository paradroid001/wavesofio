﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EntityManager : MonoBehaviour 
{
    public EntityMapping[] gameObjects; //this is for the inspector
    private Dictionary<int, EntityIO> _entities; //for efficiency
    private Dictionary<string, GameObject> _gameObjects; //active objs
    private GameNetworkSwitchboard _gamenet;


	// Use this for initialization
	void Start () 
    {
	     _entities = new Dictionary<int, EntityIO>();
        _gameObjects = new Dictionary<string, GameObject>();
        Debug.Log("Creating entity mapping from " + gameObjects.Length + " objects");
        foreach (EntityMapping em in gameObjects)
        {
            Debug.Log("Mapping " + em.ename + " to " + em.eobj.ToString() );
            _gameObjects[em.ename] = em.eobj;
        }

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnReceiveStartEntity(EntityData data)
    {
        Debug.Log("Receive start entity: " + data.uid + " type: " + data.xtra.type);
        CreateEntity(data, data.xtra.local);
    }

    public void OnReceiveDestroyEntity(EntityData data)
    {
        if (_entities.ContainsKey(data.uid))
        {
            EntityIO entity = _entities[data.uid];
            if (!entity.local)
            {
                entity.OnDestroy();
                _entities.Remove(data.uid);
            }
        }
    }

    public void OnReceiveUpdateEntity(EntityData data)
    {
        //Debug.Log("received update for " + data.uid );
        if (_entities.ContainsKey(data.uid))
        {
            EntityIO entity = _entities[data.uid];
            if (!entity.local)
            {
                //Debug.Log("Updating nonlocal entity with " + data.pos.x + ", " + data.pos.y);
            }
            entity.SyncFromNet(data);
        }
        else
            Debug.Log("Received unknown entity update: " + data.uid);
        //_entities[data.uid].data.uid = data.uid;
    }

    public void OnStartEntity()
    {
    }
    
    public void OnDestroyEntity()
    {
    }

    public void CreateEntity(EntityData data, bool isLocal = false)
    {
        //Debug.Log("Entitymap is: ");
        //foreach (string name in _gameObjects.Keys)
        //{
        //    Debug.Log("" + name + ": " + _gameObjects[name].ToString());
        //}
        GameObject g = _gameObjects[data.xtra.type];
        //Debug.Log("Entitymap g was " + g.ToString());
        if (g != null)
        {
            //Debug.Log("Creating Player");
            //_entities[data.uid] = 
            GameObject go = Instantiate(g, new Vector2(data.pos.x, data.pos.y), Quaternion.identity) as GameObject;
            //Get its entity io.
            EntityIO goeio = go.GetComponent<EntityIO>();
            if (goeio != null)
            {
                if (isLocal)
                    goeio.local = true;
                goeio.ExtraInit(data.xtra);
                //store it in the dict
                _entities[data.uid] = goeio;
                OnReceiveUpdateEntity(data);
            }
            else
            {
                Debug.LogError("Created entity (" + data.xtra.type + ") does not have Entity IO component!");
            }
        }
        else
        {
            Debug.Log("No entity type matching " + data.xtra);
        }

    }

}
