﻿using UnityEngine;
using System.Collections;

public class OnStartBehaviour : MonoBehaviour 
{
    public bool destroy = false;
    public bool deactivate = false;
    public bool onAwake = false;
    public bool onStart = false;

    void Awake()
    {
        if (onAwake)
            TakeAction();
    }

	// Use this for initialization
	void Start () 
    {
        if (onStart)
            TakeAction();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void TakeAction()
    {
        if (destroy)
            Destroy(this.gameObject);
        else
            this.gameObject.SetActive(!deactivate);
    }
}
