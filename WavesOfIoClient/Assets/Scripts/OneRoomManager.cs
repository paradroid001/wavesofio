﻿using UnityEngine;
using System.Collections;
using Pixeltron.GameUtils;

public class OneRoomManager : GameManager 
{
	/*protected override void Awake()
    {
        base.Awake();
    }*/

    public GameNetworkSwitchboard _gamenet;
    private string _playerName;

    public static OneRoomManager manager
    {
        get
        {
            return (OneRoomManager)GMS.instance.manager;
        }
    }

    // Use this for initialization
	protected override void Start () 
    {
	    base.Start();
	}
	
	// Update is called once per frame
	protected override void Update () 
    {
	    base.Update();
	}

    public string playerName
    {
        get {return _playerName;}
    }


    public void StartGame(string pname)
    {
        //Hide all menus
        ShowMenu(false); 

        //Enable the socket connection.
        _playerName = pname;
        _gamenet.gameObject.SetActive(true);
    }
}
