﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OneRoomPlayer : MonoBehaviour 
{
    public Text playerUI;
    private /*Player*/EntityIO _playerEntityIO;
    
	// Use this for initialization
	void Start () 
    {
	    _playerEntityIO = GetComponent<PlayerEntityIO>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	    playerUI.text = "UID: " + _playerEntityIO.data.uid + 
            //"\nName: " + _playerEntityIO.name +
            "\nX: " + _playerEntityIO.data.pos.x + 
            "\nY: " + _playerEntityIO.data.pos.y;
	}
}
