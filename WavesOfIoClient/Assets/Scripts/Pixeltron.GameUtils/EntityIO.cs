﻿using UnityEngine;
using System.Collections;

public class EntityIO : MonoBehaviour 
{
    protected bool _isLocal; //is this a local or a remote obj?
    protected GameNetworkSwitchboard _gamenet;
    protected EntityData _data;
    public float updaterate = 0.1f;
	// Use this for initialization
	void Awake()
    {
        //We may not need to init this?
        //We end up receiving something anyway...
        _data = new EntityData();
        _isLocal = false;
    }
    
    public virtual void Start () 
    {
	    _gamenet = GameObject.FindObjectOfType<GameNetworkSwitchboard>();
        if (updaterate > 0.0f)
            InvokeRepeating("SyncToNet", 1.0f, updaterate);
	}

    public int uid
    {
        get {return _data.uid;}
        set {_data.uid = value;}
    }

    public bool local
    {
        get {return _isLocal;}
        set {_isLocal = value;}
    }

    public EntityData data
    {
        get {return _data;}
        set {
            _data = value;
            //Debug.Log("data is now " + JsonUtility.ToJson(_data));
        }
    }

    public int parentid
    {
        get {return _data.puid;}
        set {_data.puid = value;}
    }
	
	// Update is called once per frame
	protected virtual void Update () 
    {
    }

    protected virtual void SyncToNet()
    {
        if (_isLocal)
        {
            if (_gamenet == null)
                _gamenet = GameObject.FindObjectOfType<GameNetworkSwitchboard>();

            //Send updates only if you're local
            if (_data != null && _gamenet != null)
            {
                _data.pos.x = transform.position.x;
                _data.pos.y = transform.position.y;
                _data.pos.a = 0.0f;
                _gamenet.SendEntityUpdate(_data);
            }
        }
    }
    public virtual void SyncFromNet(EntityData data)
    {
        _data = data;
        if (!_isLocal)
        {
            //Debug.Log("syncinc nonlocal entity " + data.uid + " to x: " + _data.pos.x + ", y: " + data.pos.y);
            transform.position = new Vector2(_data.pos.x, _data.pos.y);
        }

    }

    protected virtual void CreateOwnedEntity()
    {
        //override me
    }

    public virtual void ExtraInit(ExtraEntityData xtra)
    {
        //override me
    }

    public virtual void OnDestroy()
    {
        Destroy(gameObject);
    }
}
