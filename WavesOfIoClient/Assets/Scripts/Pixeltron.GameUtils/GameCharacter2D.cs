using System;
using UnityEngine;

namespace Pixeltron.GameUtils
{
    [RequireComponent (typeof (Rigidbody2D))]
    public class GameCharacter2D : MonoBehaviour
    {
        [SerializeField] private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.
        [SerializeField] private LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character

        private Animator m_Anim;            // Reference to the player's animator component.
        private Rigidbody2D m_Rigidbody2D;
        public GameObject sprite;
        public bool m_FacingRight = true;  // For determining which way the player is currently facing.

        bool dead = false;

        private Weapon currentWeapon;

        private void Awake()
        {
            // Setting up references.
            m_Anim = GetComponent<Animator>();
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
        }


        private void FixedUpdate()
        {
            // Set the vertical animation
            //m_Anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);
            //m_Anim.SetFloat("hSpeed", Mathf.Abs(m_Rigidbody2D.velocity.x));
        }


        public void Move(float h, float v, float sh, float sv, bool ability)
        {
            // Move the character
            //wrong for diagonal.
            m_Rigidbody2D.velocity = new Vector2(h*m_MaxSpeed, v*m_MaxSpeed);

            // If the input is moving the player right and the player is facing left...
            if (h > 0 && !m_FacingRight)
            {
                // ... flip the player.
                Flip();
            }
                // Otherwise if the input is moving the player left and the player is facing right...
            else if (h < 0 && m_FacingRight)
            {
                // ... flip the player.
                Flip();
            }

            if (Math.Abs(sh) > 0.1f || Math.Abs(sv) > 0.1f)
                shoot(sh, sv);
        }


        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            if (sprite != null)
                sprite.transform.localScale = theScale;
            else
                transform.localScale = theScale;
        }

        public void shoot(float sh, float sv)
        {
            if (currentWeapon)
            {
                Vector2 dir = new Vector2(sh, sv);
                dir.Normalize();
                currentWeapon.fire(dir);
            }
        }

        public void pickup(Weapon w)
        {
            if (currentWeapon)
            {
                currentWeapon.be_dropped(this);
            }
            currentWeapon = w;
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            GameObject go = other.gameObject;
            if (go.tag == "Plant")
            {
                Debug.Log("Player died");
                GMS.instance.OnPlayerDeath();
            }
        }

    }
}
