﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pixeltron.GameUtils;
using System;

[System.Serializable]
public class Entity2DPos
{
    public float x;
    public float y;
    public float a; //angle
}

[System.Serializable]
public class ExtraEntityData
{
    public string type;
    public string name;
    public bool local;
    //extend here as necessary
}

[System.Serializable]
public class EntityData
{
    public int uid;  //our uid
    public int puid; //parent uid (if any)
    public Entity2DPos pos;
    public Entity2DPos vel;
    //public EntityData()
    //{
    //    this.pos = new Entity2DPos();
    //    this.vel = new Entity2DPos();
    //}
    public ExtraEntityData xtra; //For extra data, not needed every frame
}

[System.Serializable]
public class EntityDataList
{
    public EntityData[] list;
}

[System.Serializable]
public class EntityMapping
{
    public string ename;
    public GameObject eobj;
}

public static class GameEvent
{
    public static string CONNECT = "connected";
    public static string DISCONNECT = "disconnect";
    public static string UPDATE = "updated";
    public static string CREATE = "create";
    public static string DESTROY = "destroy";
}


public class GameNetworkSwitchboard : MonoBehaviour 
{
    private SocketConnection _socket;
    public EntityManager _manager;
	// Use this for initialization
	void Start () 
    {
        Debug.Log("Starting Switchboard");
	    _socket = GameObject.FindObjectOfType<SocketConnection>();
        if (_socket != null)
        {
            //We initialise the socket with the name of this
            //object, so it knows where to pass received messages
            _socket.Init(gameObject.name);
            _socket.On("connected", OnConnected);
            _socket.On("player_disconnected", OnDisconnected);
            _socket.On("updated", OnUpdated);
            _socket.On("joined", OnJoined);
            _socket.On("created", OnCreated);
            _socket.On("destroyed", OnDestroyed);
        }
        Debug.Log("Finished Starting Switchboard");
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void OnPlayerStart(string data)
    {
        //Debug.Log("Create Local Player Here");
        //Debug.Log("String data is " + data);
        EntityData e = JsonUtility.FromJson<EntityData>(data);
        if (e != null)
        {
            //Passing true because its a local entity
            _manager.OnReceiveStartEntity(e);
        }
    }

    public void OnConnected(string data)
    {
        //Tell the server that we're connecting, and what our name is
        Send("join", "\"" + ((OneRoomManager)GMS.instance.manager).playerName + "\"");
        //Server should reply with 'joined'
    }

    private EntityData GetEntityDataFromString(string data)
    {
        EntityData e = null;
        try
        {
            e = JsonUtility.FromJson<EntityData>(data);
        }
        catch (Exception ex)
        {
            Debug.LogException(ex, this);
            Debug.Log("String was: " + data);
        }

        if (e == null)
        {
            Debug.Log("entitydata unjsoned to null.");
        }
        return e;
    }

    public void OnDisconnected(string data)
    {
        //So either we disconnected, or someone else did.
        Debug.Log("Disconnected. Choose what to do here");
        EntityData e = GetEntityDataFromString(data);
        if (e != null)
        {
            _manager.OnReceiveDestroyEntity(e);
        }

    }

    public void OnUpdated(string data)
    {
        //Debug.Log("Updated: " + data);
        
        EntityData e = GetEntityDataFromString(data);
        if (e != null)
        {
            _manager.OnReceiveUpdateEntity(e);
        }
    }

    public void OnJoined(string data)
    {
        Debug.Log("On Joined. " + data);
        OnPlayerStart(data);
    }

    //Server created something
    public void OnCreated(string data)
    {
        EntityData e = GetEntityDataFromString(data);
        if (e != null)
        {
            _manager.CreateEntity(e);
        }
    }

    //Server destroyed something
    public void OnDestroyed(string data)
    {
        EntityData e = GetEntityDataFromString(data);
        if (e != null)
        {
            _manager.OnReceiveDestroyEntity(e);
        }
    }
    
    
    public void SendEntityUpdate(EntityData data)
    {
        _socket.Send("update", JsonUtility.ToJson(data) );
    }

    public void Send(string entityname, string data)
    {
        _socket.Send(entityname, data );
    }
}
