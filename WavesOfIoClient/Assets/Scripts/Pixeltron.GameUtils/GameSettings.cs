﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour 
{

    //Music and Audio
    [SerializeField]
    private float musicVolume = -5.0f;
    [SerializeField]
    private float sFXVolume = -8.0f;
    [SerializeField]
    public float screenShakeBaseAmount = 0.5f;
    [SerializeField]
    public bool bloomOn = false;

    //Controls for various players
    //so all your player inputs can be set up here.

    //Other settings like pause button
    public string PauseInputName = "Cancel";

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
