using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Pixeltron.GameUtils
{
    [RequireComponent(typeof (GameCharacter2D))]
    public class GameUserControl2D : MonoBehaviour
    {
        private GameCharacter2D m_Character;

        public string horizontalInput = "Horizontal_P1";
        public string verticalInput = "Vertical_P1";
        public string shootHorizontalInput = "ShootHorizontal_P1";
        public string shootVerticalInput = "ShootVertical_P1";
        public string abilityInput = "Ability_P1";
        
        private bool m_UsingAbility = false;

        private void Awake()
        {
            m_Character = GetComponent<GameCharacter2D>();
        }

        private void Update()
        {
            if (!m_UsingAbility)
            {
                // Read the jump input in Update so button presses aren't missed.
                m_UsingAbility = CrossPlatformInputManager.GetButtonDown(abilityInput);
            }
        }


        private void FixedUpdate()
        {
            // Read the inputs.
            float h = CrossPlatformInputManager.GetAxis(horizontalInput);
            float v = CrossPlatformInputManager.GetAxis(verticalInput);
            float sh = CrossPlatformInputManager.GetAxis(shootHorizontalInput);
            float sv = CrossPlatformInputManager.GetAxis(shootVerticalInput);
            // Pass all parameters to the character control script.
            m_Character.Move(h, v, sh, sv, m_UsingAbility);
            m_UsingAbility = false;
        }
    }
}
