﻿using UnityEngine;
using System.Collections;

public class Flicker : MonoBehaviour {

    private Light m_Light;
    [Range(0f, 0.5f)]
    public float intensityRangeMin;
    [Range(0.5f, 8.0f)]
    public float intensityRangeMax;

    private float timer;
    private float threshold;

	// Use this for initialization
	void Start () 
    {
         m_Light = GetComponent<Light>();
         timer = 0.0f;
         threshold = 1.0f;
	}
	
	// Update is called once per frame
	void Update () 
    {
        timer += Time.deltaTime;
	    if (m_Light && timer > threshold)
        {
            m_Light.intensity = Random.value * (intensityRangeMax-intensityRangeMin) + intensityRangeMin;
            timer = 0.0f;
            threshold = Random.value / 3.0f; //new threshold.
        }
	}
}
