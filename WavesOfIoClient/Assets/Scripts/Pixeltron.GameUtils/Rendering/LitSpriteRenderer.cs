﻿using UnityEngine;
using System.Collections;

public class LitSpriteRenderer : MonoBehaviour
{
    
    public Texture normalMap;
    public Texture colourRamp;
    
    private SpriteRenderer spriteRenderer;
    private MaterialPropertyBlock materialPropertyBlock;

    public bool flipped = false; //1.0f if flipped.
    public bool receiveShadows = true;
    public bool castShadows = true;

	void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        materialPropertyBlock = new MaterialPropertyBlock();
        if (castShadows)
            GetComponent<Renderer>().shadowCastingMode =  UnityEngine.Rendering.ShadowCastingMode.On;
        if (receiveShadows)
            GetComponent<Renderer>().receiveShadows = true;
        set_properties();
    }
    
    // Use this for initialization
	void Start () 
    {
	}

    public void flip()
    {
        //Debug.Log("Flipped, now normals = " + (flipped ? -1.0f : 1.0f));
        flipped = !flipped;
        set_properties();
    }

    public void set_properties()
    {
        //Debug.Log("Setting Props for " + gameObject.name);
        materialPropertyBlock.Clear();
        materialPropertyBlock.SetTexture("_MainTex", spriteRenderer.sprite.texture);
        materialPropertyBlock.SetTexture("_NormalsTex", normalMap);
	    materialPropertyBlock.SetTexture("_CelRamp", colourRamp);
        materialPropertyBlock.SetFloat("_Flipped", flipped ? -1.0f : 1.0f);
        spriteRenderer.SetPropertyBlock(materialPropertyBlock);

    }
	
}
