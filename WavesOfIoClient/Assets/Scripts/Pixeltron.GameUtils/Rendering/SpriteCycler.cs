﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(SpriteRenderer))]
public class SpriteCycler : MonoBehaviour {
    
    [SerializeField] public float cycleTime = 0.4f;
    
    [System.Serializable]
    public struct AnimationCycle
    {
        public string name;
        public Sprite[] frames;
    }

    public string defaultAnimationName = "None";
    public AnimationCycle[] animationCycles;
    private AnimationCycle currentAnimation;
    private int currentFrameIndex;
    private float internal_timer;
    private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () 
    {
	    internal_timer = 0.0f;
        spriteRenderer = GetComponent<SpriteRenderer>();
        currentAnimation.name="Unknown";
        set_animation(defaultAnimationName);
	}
	
	// Update is called once per frame
	void Update () 
    {
	    internal_timer += Time.deltaTime;
        if (internal_timer > cycleTime)
        {
            internal_timer = 0.0f;
            if (currentFrameIndex < currentAnimation.frames.Length-1)
            {
                currentFrameIndex+= 1;
            }
            else
            {
                currentFrameIndex = 0;
            }
            //Set the frame if there is a nonzero frame set
            if (currentAnimation.name != "Unknown" && currentAnimation.frames.Length > 0)
            {
                spriteRenderer.sprite = currentAnimation.frames[currentFrameIndex];
            }
        }
	}

    public void set_animation(string name)
    {
        if (currentAnimation.name != name)
        {
            int index = find_animation(name);
            if (index >= 0)
            {
                currentAnimation.name = animationCycles[index].name;
                currentAnimation.frames = animationCycles[index].frames;
                currentFrameIndex = 0;
            }
        }
    }

    //So we can't return an AnimationCycle since its
    //not a reference type - there would be no way of 
    //returning 'null'. Instead we return an index.
    //Less than 0 means it was not found.
    int find_animation(string name)
    {
        int retval = -1;
        for (int index = 0; index < animationCycles.Length; index++)
        {
            if (animationCycles[index].name == name)
            {
                Debug.Log("Found animation " + name);
                retval = index;
                break;
            }
        }
        return retval;
    }
}
