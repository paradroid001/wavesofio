﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
    using SocketIO;
#endif
#if UNITY_STANDALONE
    using SocketIO;
#endif

public class SocketConnection : MonoBehaviour 
{

    public delegate void SocketEventHandler(string data);
    private Dictionary<string, SocketEventHandler> _handlers;
    private string _objectName; //name of obj which has event callbacks. Needed for JS SendMessage code, when running in WebGL so we get replies. Restriction is that all messages then have to be sent to the object with this name.

#if UNITY_EDITOR || UNITY_STANDALONE
    private SocketIOComponent _socket;
#endif

    private GameNetworkSwitchboard _gamenet;
	// Use this for initialization
	void Start () 
    {
    }

    	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void Init(string objname)
    {
        Debug.Log("SocketConnection init");
        _objectName = objname;
        _gamenet = GameObject.FindObjectOfType<GameNetworkSwitchboard>();
        _handlers = new Dictionary<string, SocketEventHandler>();
#if UNITY_EDITOR || UNITY_STANDALONE
	    _socket = GameObject.FindObjectOfType<SocketIOComponent>();
        _socket.enabled = true;
        //_socket.On("connected", OnRawConnected);
        //_socket.On("disconnect", OnRawDisconnected);
        //_socket.On("updated", OnRawReceiveUpdate);
#else
        Application.ExternalCall("Init");
        //Application.ExternalCall("OnEvent", "SocketIO", "connected", "OnConnected");
        //Application.ExternalCall("OnEvent", "SocketIO", "disconnect", "OnDisconnected");
        //Application.ExternalCall("OnEvent", "SocketIO", "updated", "OnReceiveUpdate");
#endif

    }

    public void On(string eventname, SocketEventHandler handler)
    {
        _handlers[eventname] = handler;
#if UNITY_EDITOR || UNITY_STANDALONE
        _socket.On(eventname, OnRawEvent);
#else
        Application.ExternalCall("OnEvent", _objectName, eventname, handler.Method.Name);
#endif
    }

#if UNITY_EDITOR || UNITY_STANDALONE
    void OnRawEvent(SocketIOEvent e)
    {
        SocketEventHandler h = _handlers[e.name];
        if (h != null)
            h(e.data.ToString());
    }
#endif

/*
#if UNITY_EDITOR || UNITY_STANDALONE
    void OnRawConnected(SocketIOEvent e)
    {
        OnConnected(e.data.ToString());
    }

    void OnRawDisconnected(SocketIOEvent e)
    {
        OnDisconnected(e.data.ToString() );
    }

    void OnRawReceiveUpdate(SocketIOEvent e)
    {
        OnReceiveUpdate(e.data.ToString() );
    }
#endif
*/

/*
    void OnConnected(string data)
    {
        Debug.Log("Connected! Data = " + data);
        _gamenet.OnPlayerStart(data);
    }

    void OnDisconnected(string data)
    {
        Debug.Log("Disconnected! Data = " + data);
    }

    void OnReceiveUpdate(string data)
    {
        Debug.Log("Update: " + data);
        EntityData ed = JsonUtility.FromJson<EntityData>(data);
        if (ed != null)
        {
            _gamenet.OnReceiveUpdateEntity(ed);
        }

    }
*/
    /*
    public void OnSendUpdate(string data)
    {
        _socket.Emit("update", new JSONObject(data) );
        //_socket.Emit("update", data);
    }
    */
    
    
    public void Send(string eventname, string data)
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        _socket.Emit(eventname, new JSONObject(data) );
#else
        Application.ExternalCall("Emit", eventname, data);
#endif
    }
    
}
