﻿using UnityEngine;
using System.Collections;

public class AutoDestroyParticles : MonoBehaviour {

	private ParticleSystem ps;
    //Value of 0 = use the autodestroy
    public float destroyAfter = 3.0f;
    private float cumulativeTime = 0.0f;
    // Use this for initialization
	void Start () 
    {
	    ps = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (destroyAfter >= 0.0f)
        {
            cumulativeTime += Time.deltaTime;
            if (cumulativeTime > destroyAfter)
            {
                Destroy(gameObject);
            }
        }
        else if ((ps != null) && !ps.IsAlive())
            Destroy(gameObject);
	}
}
