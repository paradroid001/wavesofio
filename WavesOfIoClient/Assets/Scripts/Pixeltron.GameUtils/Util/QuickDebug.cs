﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;

public class DebugValue
{
    public string value;
    public Text textComponent;
    public float lastUpdatedTime;

    public DebugValue(string val, Text txt)
    {
        set_value(val);
        textComponent = txt;
    }

    public void set_value(string newvalue)
    {
        value = newvalue;
        lastUpdatedTime = Time.time;
    }
}

public class DebugSet
{
    public string name;
    public Dictionary<string, DebugValue> infodict;
    public int slot = 0;
    public GameObject panel;

    public DebugSet(GameObject p)
    {
        panel = p;
        infodict = new Dictionary<string, DebugValue>();
    }

    public bool has_key(string key)
    {
        return infodict.ContainsKey(key);
    }

    public void set_name(string name)
    {
        panel.transform.Find("NameText").gameObject.GetComponent<Text>().text = name;
    }

    public void update_item(string key, string value)
    {
        if (!infodict.ContainsKey(key))
        {
            if (slot < 7)
            {
                Transform t = panel.transform.Find("Text"+(slot+1));
                if (t)
                {
                    DebugValue dv = new DebugValue(value, t.gameObject.GetComponent<Text>());
                    infodict.Add(key, dv);
                    slot += 1;
                }
            }
        }
        else
            infodict[key].set_value(value);
        infodict[key].textComponent.text = "" + key + ": " + value;
    }

    public void update()
    {
        foreach (string key in infodict.Keys)
        {
            DebugValue dv = infodict[key];
            dv.textComponent.color = Color.Lerp(Color.green, Color.white, (Time.time-dv.lastUpdatedTime)/3.0f);
        }
    }

    public void destroy()
    {
        GameObject.Destroy(panel);
        panel = null;
        infodict = null;
    }
}


public class QuickDebug : MonoBehaviour 
{

    static public GameObject debugCanvasRoot;
	static public bool showDebug;
    static public bool showDebugDirty; //if showDebug has changed.
	static private QuickDebug _instance;
	private Dictionary<GameObject, DebugSet > debugDict;
	
	public GameObject panelPrefab;
	public GameObject textPrefab;
    private float panelScale = 0.03f;
    public float yoffset = 3.0f;

    private List<GameObject> nullgos;
	

	public static QuickDebug instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType<QuickDebug>();
                _instance.init();
			}
			return _instance;
		}
	}
	
	// Use this for initialization
	void Start () 
	{

	}

    public void init()
    {
        showDebug = false;
		debugCanvasRoot = GameObject.Find("DebugRoot");
        debugCanvasRoot.SetActive(showDebug);
		debugDict = new Dictionary<GameObject, DebugSet>();
        nullgos = new List<GameObject>();

    }
	
	// Update is called once per frame
	void Update () 
	{
        if (CrossPlatformInputManager.GetButtonDown("showDebug"))
        {
            toggleShowDebug();
        }
        
        foreach (KeyValuePair<GameObject, DebugSet> kvp in debugDict)
        {
            if (kvp.Key != null)
            {
                if (showDebug)
                {
                    kvp.Value.update();
                    kvp.Value.panel.transform.position = new Vector3(kvp.Key.transform.position.x, kvp.Key.transform.position.y+yoffset, kvp.Key.transform.position.z);
                }
            }
            else
            {
                nullgos.Add(kvp.Key);
                kvp.Value.destroy();
            }
        }
        if (nullgos.Count > 0)
        {
            foreach (GameObject go in nullgos)
            {
                debugDict.Remove(go);
            }
            nullgos.Clear();
        }

        if (showDebugDirty)
        {
            showDebugDirty = false;
            debugCanvasRoot.SetActive(showDebug);
        }
    }

    void toggleShowDebug()
    {
        showDebug = !showDebug;
        showDebugDirty = true;
    }
	
    public void debug(Component c, string key, int value)
    {
        debug(c, key, ""+value);
    }

    public void debug(Component c, string key, float value)
    {
        debug(c, key, ""+value);
    }

    public void debug(Component c, string key, bool value)
    {
        debug(c, key, ""+value);
    }

    public void debug(Component c, string key, Object value)
    {
        debug(c, key, ""+value);
    }

	public void debug(Component c, string key, string value)
	{
        GameObject go = c.gameObject;
		if (!debugDict.ContainsKey(go) )
		{
			//make the panel
            GameObject p = Instantiate(panelPrefab, new Vector2(0, 0), Quaternion.identity ) as GameObject;
			p.transform.SetParent(debugCanvasRoot.transform, false);
            p.transform.position = new Vector2(go.transform.position.x, go.transform.position.y + yoffset);
            p.transform.localScale = new Vector3(panelScale, panelScale, 1.0f);
            //add a new debugset on that panel
            DebugSet d = new DebugSet(p);
            d.set_name(go.name);
			debugDict.Add(go, d);
		}
        debugDict[go].update_item(key, value);
	}
}
