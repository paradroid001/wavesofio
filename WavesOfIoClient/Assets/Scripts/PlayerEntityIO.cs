﻿using UnityEngine;
using System.Collections;

public class PlayerEntityIO : EntityIO 
{
    public string playername = "Unnamed";
    private Rigidbody2D _rb2d;

    
	public override void Start ()
    {
        _rb2d = GetComponent<Rigidbody2D>();
	    base.Start();
	}
	
	// Update is called once per frame
	protected override void Update () 
    {
	    base.Update();
        //Debug.Log("UID: " + _data.uid);
	}

    public override void ExtraInit(ExtraEntityData xtra)
    {
        base.ExtraInit(xtra);
        playername = xtra.name;
    }

    protected override void SyncToNet()
    {
        if (_data != null)
        {
            _data.vel.x = _rb2d.velocity.x;
            _data.vel.y = _rb2d.velocity.y;
        }
        base.SyncToNet();
    }

    public override void SyncFromNet(EntityData data)
    {
        base.SyncFromNet(data);
        if (!_isLocal)
        {
            _rb2d.velocity = new Vector2(_data.vel.x, _data.vel.y);
        }
    }
}
