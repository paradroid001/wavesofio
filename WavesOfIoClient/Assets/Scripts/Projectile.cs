﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public GameObject splashPrefab;
    [Range(1, 100)]
    public int damageAmount = 10;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public int get_damage()
    {
        return damageAmount;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        GameObject g = other.gameObject;
        //Debug.Log(g.name);
        if (g.tag == "Enemy")
        {
            Debug.Log("hit enemy");
            Destroy(gameObject);
            if (splashPrefab)
                Instantiate(splashPrefab, transform.position, Quaternion.identity);
        }
        else if (g.tag == "Player" || g.tag == "Weapon" || g.tag == "Item")
        {

        }
        else//hopefully it was a wall. Don't really want to have to tag all these
        {
            Destroy(gameObject);
            if (splashPrefab)
                Instantiate(splashPrefab, transform.position, Quaternion.identity);
        }
    }
}
