﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tests : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
    {
	    TestEntityDataSerialise();
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    void TestEntityDataSerialise()
    {
        Debug.Log("Testing Deserialize");
        string s = "{\"uid\":0,\"puid\":-1,\"pos\":{\"x\":-0.938493013381958,\"y\":0.0,\"a\":0.0},\"vel\":{\"x\":0.0,\"y\":0.0,\"a\":0.0},\"xtra\":{\"type\":\"Player\",\"name\":\"\\\"fds\\\"\",\"local\":true}}";
        EntityData e = JsonUtility.FromJson<EntityData>(s);
        Debug.Log(e.pos.x);
        Debug.Log(e.pos.y);
    }
}
