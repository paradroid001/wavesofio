﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Pixeltron.GameUtils;
public class StartGameMenu : MonoBehaviour 
{

    public Button GoButton;
    private string playerName;

	// Use this for initialization
	void Start () 
    {
	    if (GoButton != null)
            GoButton.interactable = false;
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void SetPlayerName(string pname)
    {
        if (pname != "")
        {
            //Debug.Log(pname);
            playerName = pname;
            GoButton.interactable = true;
        }
        else
        {
            GoButton.interactable = false;
        }
    }

    public void Go()
    {
        //Debug.Log("Go called with " + playerName);
        OneRoomManager.manager.StartGame(playerName);
    }
}
