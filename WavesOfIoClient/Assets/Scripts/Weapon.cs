﻿using UnityEngine;
using System.Collections;
using Pixeltron.GameUtils;


[RequireComponent (typeof (AudioSource))]
public class Weapon : MonoBehaviour 
{
    private AudioSource audioSource; 
    public GameObject bulletPrefab;
    public float fireRate; //bullets per second
    public float projectileVelocity = 10.0f;
    private float timeSinceLastFired;
    private Transform previousParent;
    private float timeSinceDropped = 0.0f;
    public float repickupTimeThreshold = 1.0f; //seconds
    [Range(0.1f, 1.0f)]
    public float shotScreenShake = 0.4f;
    public float shotScreenShakeDamping = 1.5f;

    public AudioClip castFX;

	// Use this for initialization
	void Start () 
    {
	    timeSinceLastFired = 0.0f;
        previousParent = transform.parent;
        timeSinceDropped = 0.0f;
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	    timeSinceLastFired += Time.deltaTime;
        timeSinceDropped += Time.deltaTime;
	}


    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            if (timeSinceDropped > repickupTimeThreshold)
                be_picked_up(collider.gameObject.GetComponent<GameCharacter2D>());
        }
    }

    public void fire(Vector2 dir)
    {
        if (timeSinceLastFired > (1.0f / fireRate) )
        {
            timeSinceLastFired = 0.0f;
            GameObject bullet = Instantiate(bulletPrefab, gameObject.transform.position, Quaternion.identity) as GameObject;
            bullet.GetComponent<Rigidbody2D>().AddForce(dir * projectileVelocity);
            GMS.instance.manager.ScreenShake(shotScreenShake, shotScreenShakeDamping);
            //Play SFX
            audioSource.clip = castFX;
            audioSource.Play();
        }
    }
    public void be_picked_up(GameCharacter2D d)
    {
        transform.SetParent(d.gameObject.transform);
        transform.localPosition = new Vector2(0.0f, 0.0f);
        d.pickup(this);
        Debug.Log("" + gameObject.name + " was picked up");
    }

    public void be_dropped(GameCharacter2D d)
    {
        transform.SetParent(previousParent);
        Debug.Log("" + gameObject.name + " was dropped");
        timeSinceDropped = 0.0f;
    }
}
